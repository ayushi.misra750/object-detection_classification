from os import path, makedirs, listdir
import sys

import cv2

cwd_path = path.dirname(sys.argv[0])
outputFolder = path.join(cwd_path, 'results')
if not path.exists(outputFolder):
    makedirs(outputFolder)

MODEL_NAME = 'object_detection_model'
PATH_TO_CKPT = path.join(cwd_path, MODEL_NAME, 'frozen_inference_graph.pb')
PATH_TO_LABELS = path.join(cwd_path, 'labelmap.pbtxt')

from DeepLearningDetectROI import DL_ROIdetect, Start_TF_Session

detection_graph1, sess1, image_tensor1, detection_boxes1, \
detection_scores1, detection_classes1, num_detections1 = Start_TF_Session(PATH_TO_CKPT)

min_score_needed = 0.8

# C:\Users\am143402\Downloads\technicalassessmentformachinelearningengineertdcxhy\train\images\banana_11.jpg
# C:\Users\am143402\Downloads\technicalassessmentformachinelearningengineertdcxhy\train\images\mixed_13.jpg

image_read = input("Enter the path of your file: ")
img_name = path.split(image_read)[1]
image = cv2.imread(image_read)
Image_box = image.copy()
image_copy = image.copy()

ROIdetect, classes, scores = DL_ROIdetect(sess1, image_tensor1, detection_boxes1, detection_scores1,
                                              detection_classes1,
                                              num_detections1, image, min_score_needed, image_copy, PATH_TO_LABELS, outputFolder, img_name)

if len(ROIdetect) > 0:
    class_id1 = [i for i, x in enumerate(classes) if x == 1]
    class_id2 = [i for i, x in enumerate(classes) if x == 2]
    class_id3 = [i for i, x in enumerate(classes) if x == 3]
    class_id4 = [i for i, x in enumerate(classes) if x == 4]
    ROI1 = [ROIdetect[x] for x in class_id1]
    ROI2 = [ROIdetect[x] for x in class_id2]
    ROI3 = [ROIdetect[x] for x in class_id3]
    ROI4 = [ROIdetect[x] for x in class_id4]

    if len(ROI1) > 0:
        for roi in ROI1:
            print("Prediction is: Apple")
            cv2.rectangle(Image_box, (roi[0], roi[1]),
                      (roi[0] + roi[2], roi[1] + roi[3]), [0, 0, 255])
    if len(ROI2) > 0:
        for roi in ROI2:
            print("Prediction is: Banana")
            cv2.rectangle(Image_box, (roi[0], roi[1]),
                      (roi[0] + roi[2], roi[1] + roi[3]), [0, 0, 255])
    if len(ROI3) > 0:
        for roi in ROI3:
            print("Prediction is: Mixed")
            cv2.rectangle(Image_box, (roi[0], roi[1]),
                      (roi[0] + roi[2], roi[1] + roi[3]), [0, 0, 255])
    if len(ROI4) > 0:
        for roi in ROI4:
            print("Prediction is: Orange")
            cv2.rectangle(Image_box, (roi[0], roi[1]),
                      (roi[0] + roi[2], roi[1] + roi[3]), [0, 0, 255])
    cv2.imwrite(path.join(outputFolder,
                      img_name[:-4] + "_boxes.tif"), Image_box)

    cv2.imshow("Box_image", Image_box)
    cv2.waitKey(0)