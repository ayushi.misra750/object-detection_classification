
from numpy import sum, expand_dims, argsort, int32, uint8, squeeze
from utils import visualization_utils as vis_util
from utils import label_map_util
import cv2

import tensorflow as tf


def denormalizeROI(image,ROIs,classes):
    """
    TF returns ROIs in a normalized format, this function brings them to a standard format
    :param image: Input image
    :param ROIs: detected ROIs
    :param classes: corresponding classes of ROIs
    :return ROIout, classout: denormalized ROIs and corresponding classes
    """
    height,width=image.shape[:2]
    ROIout = []
    classout = []

    for ROI,cls in zip(ROIs,classes):
        ymin=int(max(ROI[0]*height,0))
        ymax=int(min(ROI[2]*height,height))
        xmin=int(max(ROI[1]*width,0))
        xmax=int(min(ROI[3]*width,width))
        w = xmax-xmin
        h = ymax-ymin
        # if index==0:
        ROIout.append([xmin, ymin, w, h])
        classout.append(int(cls))


    return ROIout, classout

def Start_TF_Session(PATH_TO_CKPT):
    """
    Starts a TF session for the model present at the given path
    :param PATH_TO_CKPT: path to the ckpt files
    :return:
    """
    tf.set_random_seed(10)
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

        sess = tf.Session(graph=detection_graph)

        # Input tensor is the image
        image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

        detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

        detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
        detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

        # Number of objects detected
        num_detections = detection_graph.get_tensor_by_name('num_detections:0')

        # image_expanded = expand_dims(image_rgb, axis=0)

    return detection_graph, sess, image_tensor, detection_boxes, detection_scores, detection_classes, num_detections


def DL_ROIdetect(sess, image_tensor, detection_boxes,\
    detection_scores, detection_classes, num_detections, image_rgb, min_score_needed, image_copy, PATH_TO_LABELS, outputFolder, name):
    """
    Detect the ROIs and their classes using Deep Learning
    """


    # Perform detection by running the model with the image as input
    tf.set_random_seed(10)

    image_expanded = expand_dims(image_rgb, axis=0)
    (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: image_expanded})

    NUM_CLASSES = 4
    label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
                                                                use_display_name=True)
    category_index = label_map_util.create_category_index(categories)

    _, bounding_boxes = vis_util.visualize_boxes_and_labels_on_image_array(
        image_copy,
        squeeze(boxes),
        squeeze(classes).astype(int32),
        squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        line_thickness=4,
        min_score_thresh=0.8)

    img_temp = image_copy.astype(uint8)
    cv2.imwrite(outputFolder + name + "_BBox.png", img_temp)

    # find ROIs/boxes with score higher than minimum score
    idx = scores>min_score_needed
    scores1 = scores[idx]
    # if no boxes are found or if no boxes of class 1 are found, lower the score by 0.1
    if len(scores1)==0 or sum(boxes[idx]==1)==0:
        idx = scores>(min_score_needed - 0.2)
        # if sum(boxes[idx]==1)==0: # if no boxes are found or if no boxes of class 1 are found, lower the score to 0.1
        #     idx = scores > 0.7

    scores1 = scores[idx]
    ROIall = boxes[idx]
    classes = classes[idx]
    # sort the ROIs according to classes
    if len(classes)>1:
        idsort = argsort(classes)
        classes = classes[idsort]
        ROIall = ROIall[idsort]
        scores1 = scores1[idsort]

    id = classes<=6
    classes = classes[id]
    ROIall = ROIall[id]
    scores1 = scores1[id]
    ROIall, classall = denormalizeROI(image_rgb, ROIall, classes)

    return ROIall, classall, scores1
